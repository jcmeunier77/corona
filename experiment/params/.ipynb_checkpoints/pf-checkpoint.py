datcol = 'dateRep'                     # date column
geocol = 'geoId'                       # geo column (location)
cascol = ['cases', 'deaths']           # case columns (new, not cumulative)
namcol = 'countriesAndTerritories'     # full geo name
popcol = 'popData2019'                 # population column

futext = '365 days'                    # future extension of dataframe