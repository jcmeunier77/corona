geo      = 'NL'                        # geo indicator (location)

measure  = 'cases'                     # file column to use as measure
smeasure = 'Week window'               # smoothed measures
rmeasure = 'rcases'                    # remaining measures after iteration
pmeasure = 'Model'                     # projected measures summed
wmeasure = 'Wave '                     # wave name prefix, zero-leading number is added
wavenum  = 2                           # wave numbering width

sdays    = 7                           # number of days for smoothing window
firstwav = 1                           # initial wave number
popcases = 1e6                         # relative minimum from population size (one in n cases)
mincases = 2                           # absolute minimum number of cases to consider

linmax   = -1/2                        # upper bound of linear derivation
linmin   = -8/2                        # lower bound of linear derivation
betamax  = 44                          # maximum accepted beta estimate
datamin  = 3                           # minimum number of data points in spline
projmin  = 1                           # minimum cases for projected start and floor
gradmin  = 1e-6                        # minimum gradient difference for knot

infdays  = 14                          # infectious number of days
sresid   = 1                           # smoothing window for residuals

plotmin  = '20200301'                  # start date of output plots
plotmax  = '20210301'                  # stop date of output plots

earth    = {'minspan'     : 1,         # minimal length of spline
            'penalty'     : 0,         # strictness in pruning
            'endspan'     : 0,         # measures allowed remaining
            'thresh'      : 1e-9,      # improvement threshold for knot
            'check_every' : 1}         # measures considered

plot     = {'figsize'     : (16, 9),   # figure size
            'grid'        : True,      # show grid
            'kind'        : 'area',    # plot kind
            'stacked'     : False,     # stacking
            'alpha'       : 1/3}       # transparency

plnew    = 'Daily new cases for '      # title start for new measure plot
plcum    = 'Cumulative cases for '     # title start for cumulative measure plot